FROM alpine:latest
LABEL maintainer="Support<address_removed>"

RUN apk update && apk add --virtual build-dependencies build-base clang cmake libusb-dev bash make

